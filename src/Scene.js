import React from 'react';

export default class Scene {
  id;
  name;
  lightIds; //only for mocks

  recall() {

  }

  asButton(onClickCallback) {
    return (
      <div
        className="col-12 col-sm-auto mb-2 mr-4 btn-group"
        key={this.id}
      >
        <button
          className="text-left btn btn-lg btn-secondary"
          onClick={() => onClickCallback(this.id)}
        >
          <i className="fas fa-dot-circle mr-2"></i>{this.name}
        </button>
      </div>
    );
  }
}
