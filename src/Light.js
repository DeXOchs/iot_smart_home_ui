import React from 'react';

export default class Light {
  id;
  name;
  xPosition;
  yPosition;
  lowerDim;
  colors;

  switchedOn;
  brightness;
  color;

  toggle() {
    this.switchedOn = !this.switchedOn;
  }

  dimDown() {
    if (this.lowerDim != null) {
      this.brightness = this.lowerDim;
    } else {
      this.brightness = 32;
    }
    if (!this.switchedOn) this.switchedOn = true;
  }

  dimUp() {
    this.brightness = 255;
    if (!this.switchedOn) this.switchedOn = true;
  }

  setColor(colorId) {
    this.color = this.colors.find(color => color.id === colorId);
    if (!this.switchedOn) this.switchedOn = true;
  }

  powerOff() {
    this.brightness = 0;
    this.switchedOn = false;
  }

  asSvg(onClickCallback) {
    return (
      <svg
        key={this.id}
        onClick={() => onClickCallback(this.id)}
        x={Math.floor(this.xPosition * 100)}
        y={Math.floor(this.yPosition * 50)}
      >
        <circle
          cx="5" cy="5"
          r="4.75"
          fill={this.switchedOn ? '#FFC107' : '#6c757d'}
        />
        <path
          d="M4.15,7.13a.3.3,0,0,0,0,.19l.15.27a.34.34,0,0,0,.29.16h.7a.34.34,0,0,0,.29-.16l.15-.27a.25.25,0,0,0,0-.19V6.69H4.15Zm2.1-4.41A1.88,1.88,0,0,0,3.6,5.37a3,3,0,0,1,.54,1H5.86a3.06,3.06,0,0,1,.55-1A1.87,1.87,0,0,0,6.25,2.72ZM5,3.27a.85.85,0,0,0-.85.85h0A.18.18,0,0,1,4,4.3a.17.17,0,0,1-.17-.17A1.19,1.19,0,0,1,5,2.94a.15.15,0,0,1,.17.15h0A.17.17,0,0,1,5,3.27Z"
          fill={this.switchedOn ? '#000' : '#ffffff'}
        />
        <circle
          display={(this.color != null && this.switchedOn) ? 'inherit' : 'none'}
          cx="8" cy="1.75"
          r="1.25"
          stroke={this.switchedOn ? '#000' : '#6c757d'}
          stroke-width="0.2"
          fill={this.color?.asHex()}
        />
      </svg>
    );
  }

  asButton(onClickCallback, onDimDownCallback, onDimUpCallback, onColorCallback) {
    let brightnessMenu = null;
    if (this.lowerDim != null) { // light has brightness menu
      let dimDownButtonColor = this.brightness === this.lowerDim ? 'btn-warning' : 'btn-outline-secondary';
      let dimUpButtonColor = this.brightness === 255 ? 'btn-warning' : 'btn-outline-secondary';

      brightnessMenu = (
          <div className="dropdown-item btn-group">
            <button
              className={'px-4 btn ' + (this.switchedOn ? dimDownButtonColor : 'btn-outline-secondary')}
              onClick={() => onDimDownCallback(this.id)}
            >
              <i className="fas fa-moon mx-1"></i> dark
            </button>
            <button
              className={'btn ' + (this.switchedOn ? dimUpButtonColor : 'btn-outline-secondary')}
              onClick={() => onDimUpCallback(this.id)}
            >
              <i className="fas fa-sun mx-1"></i> bright
            </button>
          </div>
      );
    }

    let colorMenu = null;
    if (this.colors != null) { // light has color menu
      colorMenu = (
        <div className="dropdown-item btn-group-vertical">
          {this.colors.map(color => {
            let buttonColor = 'btn-outline-secondary';
            if (this.switchedOn && this.color.id === color.id) buttonColor = 'btn-warning';

            return (
              <button
                className={'btn text-left ' + buttonColor}
                onClick={() => onColorCallback(this.id, color.id)}
              >
                <span className="fa-stack mx-1">
                  <i className="fas fa-circle fa-stack-2x" style={{color: color.asHex()}}></i>
                  <i className="fas fa-brush fa-stack-1x fa-inverse"></i>
                </span>
                {color.name}
              </button>
            );
          })}
        </div>
      );
    }

    let dropdownButton = null;
    let dropdownMenu = null;
    if (brightnessMenu || colorMenu) {
      dropdownButton = (
        <button
          className={'text-right btn dropdown-toggle dropdown-toggle-split ' + (this.switchedOn ? 'btn-warning' : 'btn-secondary')}
          data-toggle="dropdown"
          data-reference="parent"
        >
          <span className="sr-only">Toggle Dropdown</span>
        </button>
      );
      dropdownMenu = (
        <div className="dropdown-menu bg-light">
          {colorMenu}
          {brightnessMenu}
        </div>
      );
    }

    return (
      <div
        className="col-12 col-sm-auto mb-2 mr-4 btn-group"
        key={this.id}
      >
        <button
          className={'text-left btn ' + (this.switchedOn ? 'btn-warning' : 'btn-secondary')}
          onClick={() => onClickCallback(this.id)}
        >
          <i className="fas fa-lightbulb mr-2"></i>{this.name}
        </button>
        {dropdownButton}
        {dropdownMenu}
      </div>
    );
  }
}
