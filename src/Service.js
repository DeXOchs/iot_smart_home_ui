import Light from './Light.js';
import Room from './Room.js';
import Scene from './Scene.js';
import Color from './Color.js';

export default class Service {
  roomsCache = null;
  lightsCache = null;
  scenesCache = null;
  colorsCache = null;

  getAllRoomIds() {
    if (this.roomsCache == null) this._loadRoomsCache();
    return this.roomsCache.map(room => room.id);
  }

  getRoom(roomId) {
    if (this.roomsCache == null) this._loadRoomsCache();
    return this.roomsCache.find(room => room.id === roomId);
  }

  getLight(lightId) {
    if (this.lightsCache == null) this._loadLightsCache();
    return this.lightsCache.find(light => light.id === lightId);
  }

  getScene(sceneId) {
    if (this.scenesCache == null) this._loadScenesCache();
    return this.scenesCache.find(scene => scene.id === sceneId);
  }

  getColor(colorId) {
    if (this.colorsCache == null) this._loadColorsCache();
    return this.colorsCache.find(color => color.id === colorId);
  }

  _loadRoomsCache() {
    let rawRooms = require('./config/roomconfig.json');
    this.roomsCache = rawRooms.map(rawRoom => Object.assign(new Room(), rawRoom));
  }

  _loadLightsCache() {
    let rawLights = require('./config/lightconfig.json');
    this.lightsCache = rawLights.map(rawLight => {
      let colors = rawLight.colorIds?.map(colorId => this.getColor(colorId));
      rawLight.colorIds = undefined;
      let light = Object.assign(new Light(), rawLight);
      light.colors = colors;
      light.color = colors?.find(color => color.id === light.colorId)
      return light;
    });
  }

  _loadScenesCache() {
    let rawScenes = require('./config/sceneconfig.json');
    this.scenesCache = rawScenes.map(rawScene => Object.assign(new Scene(), rawScene));
  }

  _loadColorsCache() {
    let rawColors = require('./config/colorconfig.json');
    this.colorsCache = rawColors.map(rawColor => Object.assign(new Color(), rawColor));
  }
}
