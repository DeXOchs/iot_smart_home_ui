import React from 'react';
import './App.css';
import RoomView from './RoomView.js';
import Service from './Service.js';

export default class App extends React.Component {
  service = new Service();
  roomIds;

  constructor(props) {
    super(props);
    this.roomIds = this.service.getAllRoomIds();
    this.state = {
      currentRoomId: 0
    }
  }

  render() {
    let roomNavItems = this.roomIds.map(roomId => (
      <li className="nav-item mr-2 text-left" key={roomId}>
        <a
          className={'nav-link ' + (this.state.currentRoomId === roomId ? 'active' : '')}
          onClick={() => this.changeRoom(roomId)}
          href="#"
        >
          <i className="fas fa-home"></i> {this.service.getRoom(roomId).name}
        </a>
      </li>
    ));
    let room = this.service.getRoom(this.state.currentRoomId);

    return(
      <div className="App">
        <nav className="justify-content-between navbar navbar-expand-sm navbar-dark bg-dark">
          <ul className="navbar-nav">
            {roomNavItems}
          </ul>

          <ul className="navbar-nav">
            <li>
              <a
                className="btn btn-outline-info"
                target="_blank" rel="noopener noreferrer"
                href="http://phoscon.de/app"
              >
                <i className="fas fa-tools"></i>
                <span className="d-none d-sm-inline ml-1">Go to Phoscon Interface</span>
              </a>
            </li>
          </ul>
        </nav>
        <div className="m-4">
          <RoomView
            room={room}
            lights={room.lightIds.map(lightId => this.service.getLight(lightId))}
            scenes={room.sceneIds.map(sceneId => this.service.getScene(sceneId))}
          />
        </div>
      </div>
    );
  }

  changeRoom(newRoomId) {
    this.setState((state, props) => ({
      currentRoomId: newRoomId
    }));
  }
}
