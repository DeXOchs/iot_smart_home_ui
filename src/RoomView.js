import React from 'react';
import Service from './Service.js';

export default class RoomView extends React.Component {
  service = new Service();

  constructor(props) {
    super(props);
    this.state = {
      lights: this.props.lights
    }
  }

  render() {
    return (
      <div className="container-fluid row no-gutters p-0">
        <h1 className="d-flex col-12 order-0 justify-content-start row no-gutters">
          {this.props.scenes.map(group => group.asButton(this.onSceneClicked))}
        </h1>
        <svg
          className="d-flex col-12 col-lg-8 order-2 order-sm-1 order-lg-1 my-4 my-lg-0 bg-light border shadow-lg rounded"
          viewBox={'0 0 ' + this.props.room.width + ' ' + this.props.room.height}
        >
          <svg dangerouslySetInnerHTML={{__html: this.props.room.asSvgString()}} />
          {this.props.lights.map(light => light.asSvg(this.onLightClicked))}
        </svg>
        <h3 className="d-flex col-12 col-lg-4 order-1 order-sm-2 pl-lg-2 justify-content-start align-items-start row no-gutters">
          {this.props.lights.map(light => light.asButton(this.onLightClicked, this.onDimDown, this.onDimUp, this.onColor))}
        </h3>
      </div>
    )
    /*

    */
  }

  onLightClicked = (id) => {
    let lightsCopy = this.props.lights.slice();
    lightsCopy.find(light => light.id === id).toggle();

    this.setState((state, props) => ({
      lights: lightsCopy
    }));
  }

  onDimDown = (id) => {
    let lightsCopy = this.props.lights.slice();
    lightsCopy.find(light => light.id === id).dimDown();

    this.setState((state, props) => ({
      lights: lightsCopy
    }));
  }

  onDimUp = (id) => {
    let lightsCopy = this.props.lights.slice();
    lightsCopy.find(light => light.id === id).dimUp();

    this.setState((state, props) => ({
      lights: lightsCopy
    }));
  }

  onColor = (lightId, colorId) => {
    let lightsCopy = this.props.lights.slice();
    lightsCopy.find(light => light.id === lightId).setColor(colorId);

    this.setState((state, props) => ({
      lights: lightsCopy
    }));
  }

  onSceneClicked = (id) => {
    this.props.scenes.find(scene => scene.id === id).lightIds
      .forEach(lightId => this.onDimUp(lightId));
  }
}
