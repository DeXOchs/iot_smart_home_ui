export default class Room {
  id;
  name;
  width;
  height;
  #svgString;
  lightIds;
  sceneIds;

  asSvgString() {
    return this.svgString;
  }
}
