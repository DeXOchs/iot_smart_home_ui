export default class Color {
  name;
  rgb;

  asHex() {
    return "#" + this._toHex(this.rgb[0]) + this._toHex(this.rgb[1]) + this._toHex(this.rgb[2]);
  }

  _toHex(number) {
    let s = Number(number).toString(16);
    if ((s.length % 2) > 0) return "0" + s;
    else return s;
  }
}
